package id.museumnasional.ticket;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.blikoon.qrcodescanner.QrCodeActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import id.museumnasional.ticket.app.AppConfig;
import id.museumnasional.ticket.app.AppController;
import id.museumnasional.ticket.helper.SQLiteHandler;

public class ScanActivity extends AppCompatActivity {
    private static final int REQUEST_CODE_QR_SCAN = 101;
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int MY_CAMERA_REQUEST_CODE = 100;
    private static final String LOGTAG = "hasil scan" ;
    private ProgressDialog pDialog;
    private SQLiteHandler db;
    String voucherTitleValue, voucherQuantityValue;
    View dialogView;
    TextView alertTitle, alertDesc, alertButton, scanButton, voucherTitle, voucherQuantity;
    ImageView alertImageSukses, alertImageGagal;
    View line;
    private String userId;
    String EXTRA_CODE = "";
    String EXTRA_URL = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);
        if(getIntent().getExtras().getString("EXTRA_CODE").equalsIgnoreCase("scan_masuk")){
            EXTRA_URL = AppConfig.URL_ORDER_SCAN_MASUK;
            EXTRA_CODE = "Scan Tiket Masuk Berhasil";
        } else {
            EXTRA_URL = AppConfig.URL_ORDER_SCAN_KELUAR;
            EXTRA_CODE = "Scan Tiket Keluar Berhasil";
        }
        alertTitle = findViewById(R.id.tv_alert_title);
        alertDesc= findViewById(R.id.tv_alert_desc);
        alertButton = findViewById(R.id.tv_alert_button);
        scanButton = findViewById(R.id.tv_scan);
        voucherQuantity = findViewById(R.id.tv_quantity);
        voucherTitle = findViewById(R.id.tv_title);
        alertImageSukses = findViewById(R.id.iv_alert_image_sukses);
        alertImageGagal = findViewById(R.id.iv_alert_image_gagal);
        line = findViewById(R.id.line);
        db = new SQLiteHandler(ScanActivity.this);
        HashMap<String, String> user = db.getUserDetails();
        pDialog = new ProgressDialog(ScanActivity.this);
        pDialog.setCancelable(false);
        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(ScanActivity.this, new String[]{Manifest.permission.CAMERA}, MY_CAMERA_REQUEST_CODE);}
        else {
            Intent i = new Intent(ScanActivity.this,QrCodeActivity.class);
            startActivityForResult( i,REQUEST_CODE_QR_SCAN);
            //getContent("KG-FO0015");
            ScanActivity.this.overridePendingTransition(R.transition.bottom_up, R.transition.nothing);
            DialogForm("Scan QR Dibatalkan!", "Proses scan anda dibatalkan.", false);
        }

        //DialogForm("Scan QR Dibatalkan!", "Transaksi anda gagal di proses, silahkan mencoba lagi.", false);

    }

    @Override

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == MY_CAMERA_REQUEST_CODE) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                Toast.makeText(this, "camera permission granted", Toast.LENGTH_LONG).show();
                Intent i = new Intent(ScanActivity.this,QrCodeActivity.class);
                startActivityForResult( i,REQUEST_CODE_QR_SCAN);

            } else {

                Toast.makeText(this, "camera permission denied", Toast.LENGTH_LONG).show();
                this.finish();

            }

        }}

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) {
            Log.d(LOGTAG, "COULD NOT GET A GOOD RESULT.");
            if (data == null) {
                DialogForm("Scan QR Dibatalkan!", "Proses scan anda dibatalkan.", false);
                alertTitle.setTextColor(getResources().getColor(R.color.red));
                return;
            }

            //Getting the passed result
            String result = data.getStringExtra("com.blikoon.qrcodescanner.error_decoding_image");
            if (result != null) {
                DialogForm("Scan QR Gagal!", "Transaksi anda gagal di proses, silahkan mencoba lagi.", false);
                alertTitle.setTextColor(getResources().getColor(R.color.red));
            }

            //return;

        } else if (requestCode == REQUEST_CODE_QR_SCAN) {
            if (data == null)
                return;
            //Getting the passed result
            String result = data.getStringExtra("com.blikoon.qrcodescanner.got_qr_scan_relult");
            Log.d(LOGTAG, "Have scan result in your app activity :" + result);
            getContent(result);

        } else {
            DialogForm("Scan QR Gagal!", "Transaksi anda gagal di proses, silahkan mencoba lagi.", false);
        }
    }

    public void getContent(String value) {
        // Tag used to cancel the request
        userId = value;
        HashMap<String, String> user = db.getUserDetails();
        String tag_string_req = "banner_req";
        pDialog.setMessage("Loading ...");
        showDialog();
        StringRequest strReq = new StringRequest(Request.Method.POST,
                EXTRA_URL, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                // Log.d(TAG, "Login Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean success = jObj.getBoolean("success");
                    //int count = jObj.getInt("count");

                    // Check for error node in json
                    if (success ) {

                       // JSONArray feedArray = jObj.getJSONArray("data");

                       // for (int i = 0; i < feedArray.length(); i++) {
                         //   JSONObject feedObj = (JSONObject) feedArray.get(i);
                           // voucherTitleValue = feedObj.getString("promo_title");
                            //voucherQuantityValue = "QUANTITY : " + String.valueOf(feedObj.getInt("quantity"));
                            //DialogForm("Scan QR Berhasil!",
                            //        "Transaksi berhasil di proses, detail transaksi akan dikirimkan melalui email.", true);
                            //alertTitle.setTextColor(getResources().getColor(R.color.green));

                        //}
                        if(!jObj.getString("data").equalsIgnoreCase("0")){
                            voucherTitleValue = "No. Tiket : " + value;
                            voucherQuantityValue = "JUMLAH TIKET : " + jObj.getString("data");
                            DialogForm(EXTRA_CODE,
                                    "Transaksi berhasil di proses, detail transaksi dapat dilihat melalui sistem administrasi Museum Nasional..", true);
                            alertTitle.setTextColor(getResources().getColor(R.color.green));
                        } else {
                            DialogForm("Scan QR Gagal!", "Kode tiket tidak ditemukan, silahkan periksa kembali tiket anda", false);
                        }


                    } else {
                        //swipeContainer.setRefreshing(false);
                        String errorMsg = jObj.getString("message");
                        DialogForm("Scan QR Gagal!", errorMsg, false);
                        alertTitle.setTextColor(getResources().getColor(R.color.red));
                    }
                } catch (JSONException e) {
                    hideDialog();
                    // JSON error
                    e.printStackTrace();
                    //swipeContainer.setRefreshing(false);
                    voucherTitleValue = EXTRA_CODE;
                    voucherQuantityValue = "QUANTITY : ";
                    DialogForm("Scan QR Berhasil!",
                            "Transaksi berhasil di proses, detail transaksi dapat dilihat melalui sistem administrasi Museum Nasional..", true);
                    alertTitle.setTextColor(getResources().getColor(R.color.green));
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                hideDialog();
                voucherTitleValue = "No. Tiket : " + value;
                voucherQuantityValue = "JUMLAH TIKET : ";
                DialogForm(EXTRA_CODE,
                        "Transaksi berhasil di proses, detail transaksi dapat dilihat melalui sistem administrasi Museum Nasional.", true);
                alertTitle.setTextColor(getResources().getColor(R.color.green));
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                HashMap<String, String> user = db.getUserDetails();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("Authorization", user.get("auth"));
                return headers;
            }
            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("no_order", value);
                return params;
            }


        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void DialogForm(String title, String desc, Boolean isSuccess) {

        if(isSuccess){
            //mFirebaseDatabase = FirebaseDatabase.getInstance();
            //mDatabaseReference = mFirebaseDatabase.getReferenceFromUrl("https://api-5446341338284205821-886526.firebaseio.com/voucher");
            //String user = userId.substring(0, userId.length()-7);
            //mDatabaseReference.child("otp"+user)
              //      .setValue("true");

            alertImageSukses.setVisibility(View.VISIBLE);
            alertImageGagal.setVisibility(View.GONE);
            voucherTitle.setVisibility(View.VISIBLE);
            voucherQuantity.setVisibility(View.VISIBLE);
            scanButton.setVisibility(View.VISIBLE);
            alertButton.setVisibility(View.VISIBLE);
            voucherTitle.setText(voucherTitleValue);
            voucherQuantity.setText(voucherQuantityValue);
        } else {
            alertImageSukses.setVisibility(View.GONE);
            alertImageGagal.setVisibility(View.VISIBLE);
            voucherTitle.setVisibility(View.GONE);
            voucherQuantity.setVisibility(View.GONE);
            scanButton.setVisibility(View.VISIBLE);
            alertButton.setVisibility(View.VISIBLE);
        }

        alertTitle.setText(title);
        alertDesc.setText(desc);

        alertButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ScanActivity.this.finish();
            }
        });
        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ScanActivity.this,QrCodeActivity.class);
                startActivityForResult( i,REQUEST_CODE_QR_SCAN);
                //getContent("KG-FO0015");
                ScanActivity.this.overridePendingTransition(R.transition.bottom_up, R.transition.nothing);
            }
        });
      //  dialog.show();
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

}

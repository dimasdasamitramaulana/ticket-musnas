package id.museumnasional.ticket;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import androidx.appcompat.app.AppCompatActivity;
import es.rcti.printerplus.printcom.models.PrintTool;
import es.rcti.printerplus.printcom.models.StructReport;

public class ContructionActivity extends AppCompatActivity {

    private Button tiket, back;
    private EditText code;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contruction);
        tiket = findViewById(R.id.ticket);
        code = findViewById(R.id.code);
        back = findViewById(R.id.back);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        tiket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String codeTicket = code.getText().toString();
                boolean cancel = false;
                View focusView = null;
                if (TextUtils.isEmpty(codeTicket)) {
                    code.setError(getString(R.string.error_field_required));
                    focusView = code;
                    cancel = true;
                }
                if (cancel) {
                    focusView.requestFocus();
                } else {
                    printTicket(codeTicket);
                }
            }
        });
    }

    private void  printTicket(String code){
        StructReport msr = new StructReport();
        msr.addItemAlignment( StructReport.ALIGNMENT_CENTER );
        msr.addItemSizeFont(StructReport.SIZE_FONT_2);
        msr.addText("MUSEUM NASIONAL INDONESIA\n");
        msr.addItemSizeFont(StructReport.SIZE_FONT_1);
        msr.addText("jl. Medan Merdeka Barat No.12 - Jakarta, Indonesia");
        msr.addText("021 3868172 - 0813 5000 7257\n");
        //msr.addBarcodeHRI( StructReport.BARCODE_HRI_BELOW );
        //msr.addBarcodeData( "1234567890128" );
        // Toast.makeText(FullscreenActivity.this, webView.getUrl() , Toast.LENGTH_SHORT).show();
        Bitmap QRBit = printQRCode(code);
        msr.addImageBitmap(QRBit);
        msr.addItemSizeFont(StructReport.SIZE_FONT_2);
        msr.addText(code + "\n");

        msr.addItemAlignment(StructReport.ALIGNMENT_CENTER);
        msr.addText("www.museumnasional.or.id\n\n\n");
        msr.addCut();
        PrintTool.sendOrder(ContructionActivity.this, msr);
    }

    private Bitmap printQRCode(String textToQR){
        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        try {
            BitMatrix bitMatrix = multiFormatWriter.encode(textToQR, BarcodeFormat.QR_CODE,300,300);
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
            return bitmap;
        } catch (WriterException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void onBackPressed() {

        finish();
        overridePendingTransition(R.transition.nothing, R.transition.bottom_down);

    }
}

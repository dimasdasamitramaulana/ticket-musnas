package id.museumnasional.ticket;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import at.blogc.android.views.ExpandableTextView;
import id.museumnasional.ticket.adapter.PromoImageViewAdapter;
import id.museumnasional.ticket.adapter.ViewImagePagerAdapter;
import id.museumnasional.ticket.app.AppConfig;
import id.museumnasional.ticket.app.AppController;
import id.museumnasional.ticket.data.BannerItems;
import id.museumnasional.ticket.data.PromoItems;
import id.museumnasional.ticket.helper.SQLiteHandler;
import id.museumnasional.ticket.helper.SessionManager;

public class PromoDetailActivity extends AppCompatActivity implements View.OnClickListener, Toolbar.OnMenuItemClickListener{
    private static final String TAG = MainActivity.class.getSimpleName();
    private RecyclerView ImageThumbView, contentOneView;
    private ViewPager bannerView;
    private List<BannerItems> ImageThumItems, bannerItems;
    private ViewImagePagerAdapter bannerAdapter;
    private List<PromoItems> promoItems;
    private PromoImageViewAdapter ImageThumbAdapter;
    private TextView contentTitle, contentDesc, contentPrice, contentBasicPrice, contentCashback,
            contentDetail, quantity, sold, counterDisplay, totalPrice, contentCategories, cashbackValue, btnBuy;
    private Integer totalPiceValue, priceValue;
    private ImageView productImage;
    private ImageView btnAdd, btnRemove;
    private int counterData = 1;
    private String shareTitle;
    private SQLiteHandler db;
    private SessionManager session;
    private String promo_id, promo_title,promo_merchant, promo_merchant_id, promo_image, promo_address, promo_price, cashback_value,
            promo_slug, promo_quantity, user_cashback;
    private ExpandableTextView contentSnk, contentBenefit;
    private LinearLayout menuWishlist, menuShare;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promo_detail);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        toolbar.inflateMenu(R.menu.toolbar_menu);
        toolbar.setOnMenuItemClickListener(this);

        //Session
        db = new SQLiteHandler(PromoDetailActivity.this);
        HashMap<String, String> user = db.getUserDetails();
        session = new SessionManager(PromoDetailActivity.this);

        bannerView = (ViewPager) findViewById(R.id.rv_banner);
        productImage = (ImageView) findViewById(R.id.iv_content_image);
        contentTitle = (TextView) findViewById(R.id.iv_content_title);
        contentDesc = (TextView) findViewById(R.id.iv_content_desc);
        contentBasicPrice = (TextView) findViewById(R.id.iv_content_basic_price);
        contentPrice = (TextView) findViewById(R.id.iv_content_price);
        cashbackValue = findViewById(R.id.iv_content_cashback_value);
        contentCashback = (TextView) findViewById(R.id.iv_content_cashback);
        menuWishlist = findViewById(R.id.menu_wishlist);
        menuWishlist.setOnClickListener(this);
        menuShare = findViewById(R.id.menu_share);
        menuShare.setOnClickListener(this);
        //quantity = (TextView) findViewById(R.id.tv_quantity);
        //sold = (TextView) findViewById(R.id.tv_sold);
        ImageThumbView = (RecyclerView)findViewById(R.id.rv_image);
        counterDisplay = (TextView) findViewById(R.id.tv_counter_display);
        contentOneView=(RecyclerView) findViewById(R.id.rv_content_1);
        btnAdd = (ImageView) findViewById(R.id.btnAdd);
        contentDetail = findViewById(R.id.tv_promo_detail);
        contentBenefit = findViewById(R.id.tv_promo_benefit);
        contentCategories = findViewById(R.id.tv_categories);
        totalPrice = findViewById(R.id.total);
        btnAdd.setOnClickListener(this);
        btnRemove = (ImageView) findViewById(R.id.btnRemove);
        btnRemove.setOnClickListener(this);
        contentSnk = (ExpandableTextView) this.findViewById(R.id.tv_snk);
        contentBenefit = findViewById(R.id.tv_promo_benefit);
        btnBuy = findViewById(R.id.btn_buy);
        btnBuy.setOnClickListener(this);

        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        setupDetail();
        setupBanner();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnAdd:
                counterData++;
                counterDisplay.setText(String.valueOf(counterData));
                totalPiceValue += priceValue;
                totalPrice.setText(String.valueOf("Rp. " + moneyFormater(String.valueOf(totalPiceValue))));
                break;

            case R.id.btnRemove:
                if(counterData <= 1){
                    System.out.println("error");
                } else {
                    counterData--;
                    counterDisplay.setText(String.valueOf(counterData));
                    totalPiceValue -= priceValue;
                    totalPrice.setText(String.valueOf("Rp. " + moneyFormater(String.valueOf(totalPiceValue))));
                }
                break;

            case R.id.menu_wishlist:
                if (session.isLoggedIn()) {
                    addWishlist();
                } else {
                    Intent intent = new Intent(PromoDetailActivity.this, LoginActivity.class);
                    startActivityForResult(intent, 1);
                    PromoDetailActivity.this.overridePendingTransition(R.transition.bottom_up, R.transition.nothing);
                }

                break;


            case R.id.menu_share:
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, shareTitle);
                String url = "https://rimember.id/details/promo/Bandung/" + getIntent().getExtras().getString("slug") ;
                sharingIntent.putExtra(Intent.EXTRA_TEXT, url);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
                break;

            case R.id.btn_buy:
                checkSession();
                break;

        }
    }

    public void setupDetail(){
        contentSnk.setText(getIntent().getExtras().getString("snk"));
        final LinearLayout viewDesc = (LinearLayout) findViewById(R.id.view_snk);
        final ImageView iconDesc = (ImageView) findViewById(R.id.icon_snk);
        contentSnk.setAnimationDuration(750L);
        contentSnk.setInterpolator(new OvershootInterpolator());
        contentSnk.setExpandInterpolator(new OvershootInterpolator());
        contentSnk.setCollapseInterpolator(new OvershootInterpolator());
        viewDesc.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(final View v)
            {
                iconDesc.setImageResource(contentSnk.isExpanded() ?
                        R.drawable.ic_keyboard_arrow_down_black_24dp : R.drawable.ic_keyboard_arrow_up_black_24dp);
                contentSnk.toggle();
            }
        });
        viewDesc.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(final View v)
            {
                if (contentSnk.isExpanded())
                {
                    contentSnk.collapse();
                    iconDesc.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp);
                }
                else
                {
                    contentSnk.expand();
                    iconDesc.setImageResource(R.drawable.ic_keyboard_arrow_up_black_24dp);
                }
            }
        });


        final LinearLayout viewBenefit = (LinearLayout) findViewById(R.id.view_benefit);
        final ImageView iconBenefit = (ImageView) findViewById(R.id.icon_benefit);
        contentBenefit.setText(getIntent().getExtras().getString("benefit"));
        contentBenefit.setAnimationDuration(750L);
        contentBenefit.setInterpolator(new OvershootInterpolator());
        contentBenefit.setExpandInterpolator(new OvershootInterpolator());
        contentBenefit.setCollapseInterpolator(new OvershootInterpolator());
        viewBenefit.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(final View v)
            {
                iconBenefit.setImageResource(contentSnk.isExpanded() ?
                        R.drawable.ic_keyboard_arrow_down_black_24dp : R.drawable.ic_keyboard_arrow_up_black_24dp);
                contentBenefit.toggle();
            }
        });
        viewBenefit.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(final View v)
            {
                if (contentBenefit.isExpanded())
                {
                    contentBenefit.collapse();
                    iconBenefit.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp);
                }
                else
                {
                    contentBenefit.expand();
                    iconBenefit.setImageResource(R.drawable.ic_keyboard_arrow_up_black_24dp);
                }
            }
        });

    }

    public void setupBanner(){
        ImageThumItems = new ArrayList<BannerItems>();
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(PromoDetailActivity.this, LinearLayoutManager.HORIZONTAL, false);
        ImageThumbView.setLayoutManager(layoutManager);
        ImageThumbAdapter = new PromoImageViewAdapter(PromoDetailActivity.this, ImageThumItems);
        ImageThumbView.setAdapter(ImageThumbAdapter);
        bannerItems = new ArrayList<BannerItems>();
        LinearLayoutManager layoutManager2
                = new LinearLayoutManager(PromoDetailActivity.this, LinearLayoutManager.HORIZONTAL, false);
        //bannerView.setLayoutManager(layoutManager);
        bannerAdapter = new ViewImagePagerAdapter(PromoDetailActivity.this, bannerItems);
        bannerView.setAdapter(bannerAdapter);
        promo_slug = getIntent().getExtras().getString("slug");
        promo_merchant = getIntent().getExtras().getString("merchant_title");
        getDataBanner(getIntent().getExtras().getString("slug"));
    }

    private void getDataBanner(String slug) {
        // Tag used to cancel the request
        String tag_string_req = "banner_req";

        StringRequest strReq = new StringRequest(Request.Method.GET,
                AppConfig.BASE_URL + "product/" + slug , new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Login Response: " + response.toString());

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean success = jObj.getBoolean("success");

                    // Check for error node in json
                    if (success) {
                        JSONArray feedArray = jObj.getJSONArray("data");

                        for (int i = 0; i < feedArray.length(); i++) {
                            JSONObject feedObj = (JSONObject) feedArray.get(i);
                            //item.setId(feedObj.getString("id"));
                            promo_id = feedObj.getString("id");
                            promo_title = feedObj.getString("promo_title");
                            promo_address = String.valueOf(feedObj.getString("merchant_address") + ", "
                                    + feedObj.getString("merchant_location"));
                            promo_price = feedObj.getString("promo_price");
                            promo_merchant_id = feedObj.getString("fid_merchant");
                            cashback_value = feedObj.getString("max_cashback");

                            contentTitle.setText(feedObj.getString("promo_title"));
                            shareTitle = feedObj.getString("promo_title");
                            contentDesc.setText(String.valueOf(feedObj.getString("merchant_address") + ", "
                                    + feedObj.getString("merchant_location")));
                            contentPrice.setText(String.valueOf("Rp. " + moneyFormater(feedObj.getString("promo_price"))));
                            contentBasicPrice.setText(String.valueOf("Rp. " + moneyFormater(feedObj.getString("basic_price"))));
                            contentBasicPrice.setPaintFlags(contentBasicPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                            cashbackValue.setText(String.valueOf("Cashback up to\nRp. " + moneyFormater(feedObj.getString("max_cashback"))));
                            contentCashback.setText(String.valueOf( "Cashback "+feedObj.getString("cashback_percentage") + " %"));
                            //sold.setText(String.valueOf(feedObj.getString("soldout") + "\n" + "terjual"));
                            //quantity.setText(String.valueOf(feedObj.getString("quantity") + "\n" + "tersisa"));
                            contentCategories.setText(feedObj.getString("category"));

                            priceValue = feedObj.getInt("promo_price");
                            totalPiceValue = priceValue;
                            totalPrice.setText(String.valueOf("Rp. " + moneyFormater(String.valueOf(priceValue))));
                            contentDetail.setText(feedObj.getString("promo_desc"));
                            JSONObject imageObj = (JSONObject) feedObj.getJSONObject("images");
                            JSONArray thumbnail = (JSONArray) imageObj.getJSONArray("thumbImage");
                            Glide.with(getApplicationContext())  //2
                                    .load(AppConfig.MERCHANT_PROFILE_URL + String.valueOf(feedObj.getString("merchant_image"))) //3
                                    .centerCrop() //4
                                    .placeholder(R.drawable.placeholder) //5
                                    .error(R.drawable.placeholder) //6
                                    //.fallback(R.drawable.ic_no_image) //7
                                    .into(productImage);
                            JSONArray bigImage = (JSONArray) imageObj.getJSONArray("bigImage");

                            for (int j = 0; j < bigImage.length(); j++) {
                                BannerItems item = new BannerItems();
                                item.setImageUrl(AppConfig.MERCHANT_BASE_URL + String.valueOf(bigImage.get(j)));
                                promo_image = AppConfig.MERCHANT_BASE_URL + String.valueOf(bigImage.get(0));
                                ImageThumItems.add(item);
                                bannerItems.add(item);
                            }

                        }

                        ImageThumbAdapter.notifyDataSetChanged();
                        bannerAdapter.notifyDataSetChanged();
                    } else {
                        //swipeContainer.setRefreshing(false);
                        String errorMsg = jObj.getString("message");
                        //Toast.makeText(getContext(),
                        //      errorMsg , Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(PromoDetailActivity.this, "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                    //swipeContainer.setRefreshing(false);
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Banner error: " + error.getMessage());
                Toast.makeText(PromoDetailActivity.this,
                        error.getMessage(), Toast.LENGTH_LONG).show();
                // hideDialog();
                //swipeContainer.setRefreshing(false);
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }




    public void addWishlist(){
        // Tag used to cancel the request
        String tag_string_req = "wishlist";

        pDialog.setMessage("Harap tunggu");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_ADD_WISHLIST, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Login Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean success = jObj.getBoolean("success");

                    // Check for error node in json
                    if (success) {
                        // user successfully logged in
                        // Create login session
                        // JSONArray feedArray = jObj.getJSONArray("data");
                        AlertDialog.Builder builder = new AlertDialog.Builder(PromoDetailActivity.this);
                        builder.setCancelable(true);
                        builder.setMessage("Promo berhasil ditambahkan ke wishlist kamu");
                        builder.setPositiveButton("Kembali",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                });

                        AlertDialog dialog = builder.create();
                        dialog.show();

                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("message");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                HashMap<String, String> user = db.getUserDetails();
                Map<String, String> params = new HashMap<String, String>();
                params.put("fid_user", user.get("uid"));
                params.put("fid_promo", promo_id);

                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                HashMap<String, String> user = db.getUserDetails();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("Authorization", user.get("auth"));
                return headers;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()){
            case R.id.toolbar_share:
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, shareTitle);
                String url = "https://rimember.id/details/promo/Bandung/" + getIntent().getExtras().getString("slug") ;
                sharingIntent.putExtra(Intent.EXTRA_TEXT, url);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));

                break;


        }
        return false;
    }

    public String moneyFormater (String value){
        try {
            NumberFormat formatter = new DecimalFormat("#,###");
            String formattedNumber = formatter.format(Double.parseDouble(value));
            return formattedNumber;
        } catch (Exception e) {
            return value ;
        }
    }

    private void checkSession(){
        if (session.isLoggedIn()) {
            PromoDetailActivity.this.overridePendingTransition(R.transition.bottom_up, R.transition.nothing);
        } else {
            Intent intent = new Intent(PromoDetailActivity.this, LoginActivity.class);
            startActivityForResult(intent, 1);
            PromoDetailActivity.this.overridePendingTransition(R.transition.bottom_up, R.transition.nothing);
        }
    }



    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}

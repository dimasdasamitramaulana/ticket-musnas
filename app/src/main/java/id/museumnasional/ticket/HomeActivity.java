package id.museumnasional.ticket;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.VideoView;

public class HomeActivity extends AppCompatActivity {

    VideoView vid;
    private LinearLayout btnOrder, btnPrint;
    private int stopPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        vid = (VideoView)findViewById(R.id.videoView);
        vid.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.videoplayback));
        vid.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });
        vid.start();
        btnOrder = findViewById(R.id.btn_order);
        btnPrint = findViewById(R.id.btn_print);
        btnOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, FullscreenActivity.class);
                startActivityForResult(intent, 1);
                HomeActivity.this.overridePendingTransition(R.transition.bottom_up, R.transition.nothing);
            }
        });
        btnPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, ContructionActivity.class);
                vid.pause();
                startActivityForResult(intent, 1);
                HomeActivity.this.overridePendingTransition(R.transition.bottom_up, R.transition.nothing);
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        stopPosition = vid.getCurrentPosition(); //stopPosition is an int
        vid.pause();
    }
    @Override
    public void onResume() {
        super.onResume();
        vid.seekTo(stopPosition);
        vid.start(); //Or use resume() if it doesn't work. I'm not sure
    }
}
package id.museumnasional.ticket;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import at.blogc.android.views.ExpandableTextView;
import id.museumnasional.ticket.adapter.ContentThreeViewAdapter;
import id.museumnasional.ticket.adapter.PromoImageViewAdapter;
import id.museumnasional.ticket.app.AppConfig;
import id.museumnasional.ticket.app.AppController;
import id.museumnasional.ticket.data.BannerItems;
import id.museumnasional.ticket.data.PromoItems;


public class MerchantDetailActivity extends AppCompatActivity implements Toolbar.OnMenuItemClickListener{
    private static final String TAG = MainActivity.class.getSimpleName();
    private RecyclerView bannerView, contentOneView, contentThreeView;
    private List<BannerItems> bannerItems;
    private List<PromoItems> promoItems, contentThreeItems;
    private PromoImageViewAdapter bannerAdapter;
    private ContentThreeViewAdapter contentThreeAdapter;
    RecyclerView.LayoutManager recyclerViewLayoutManager1,recyclerViewLayoutManager2;
    private TextView contentTitle, contentAddress;
    private Integer totalPiceValue, priceValue;
    private ImageView productImage;
    private ImageView btnAdd, btnRemove, rvBanner;
    private int counterData = 1;
    private String shareTitle;

    private ExpandableTextView contentDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_merchant_detail);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        toolbar.inflateMenu(R.menu.toolbar_menu_merchant);
        toolbar.setOnMenuItemClickListener(this);

        productImage = (ImageView) findViewById(R.id.iv_content_image);
        contentTitle = (TextView) findViewById(R.id.iv_content_title);
        contentAddress = (TextView) findViewById(R.id.iv_content_adress);
        bannerView = (RecyclerView)findViewById(R.id.rv_image);
        contentOneView=(RecyclerView) findViewById(R.id.rv_content_1);
        contentDetail = (ExpandableTextView) this.findViewById(R.id.tv_content_desc);
        rvBanner = findViewById(R.id.rv_banner);
        setupDetail();
        setupBanner();

        contentThreeView=(RecyclerView) findViewById(R.id.rv_content_3);
        setupContentThree();
    }


    public void setupDetail(){
        contentDetail.setText(getIntent().getExtras().getString("desc"));
        final LinearLayout viewDesc = (LinearLayout) findViewById(R.id.view_content_desc);
        final ImageView iconDesc = (ImageView) findViewById(R.id.icon_content_desc);
        contentDetail.setAnimationDuration(750L);
        contentDetail.setInterpolator(new OvershootInterpolator());
        contentDetail.setExpandInterpolator(new OvershootInterpolator());
        contentDetail.setCollapseInterpolator(new OvershootInterpolator());
        viewDesc.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(final View v)
            {
                iconDesc.setImageResource(contentDetail.isExpanded() ?
                        R.drawable.ic_keyboard_arrow_down_black_24dp : R.drawable.ic_keyboard_arrow_up_black_24dp);
                contentDetail.toggle();
            }
        });
        viewDesc.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(final View v)
            {
                if (contentDetail.isExpanded())
                {
                    contentDetail.collapse();
                    iconDesc.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp);
                }
                else
                {
                    contentDetail.expand();
                    iconDesc.setImageResource(R.drawable.ic_keyboard_arrow_up_black_24dp);
                }
            }
        });


    }

    public void setupBanner(){
        bannerItems = new ArrayList<BannerItems>();
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(MerchantDetailActivity.this, LinearLayoutManager.HORIZONTAL, false);
        bannerView.setLayoutManager(layoutManager);
        bannerAdapter = new PromoImageViewAdapter(MerchantDetailActivity.this, bannerItems);
        bannerView.setAdapter(bannerAdapter);
        getDataBanner(getIntent().getExtras().getString("id"));
        //getDataBanner("59");
    }

    private void getDataBanner(String slug) {
        // Tag used to cancel the request
        String tag_string_req = "banner_req";

        StringRequest strReq = new StringRequest(Request.Method.GET,
                AppConfig.URL_ADD_WISHLIST + "id=" + slug + "/page=1/limit=10/sort=desc" + slug, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Login Response: " + response.toString());

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean success = jObj.getBoolean("success");

                    // Check for error node in json
                    if (success) {
                        JSONArray feedArray = jObj.getJSONArray("data");

                        for (int i = 0; i < feedArray.length(); i++) {
                            JSONObject feedObj = (JSONObject) feedArray.get(i);
                            //item.setId(feedObj.getString("id"));
                            contentTitle.setText(feedObj.getString("title"));
                            shareTitle = feedObj.getString("title");
                            contentAddress.setText(String.valueOf(feedObj.getString("address") + ", "
                                    + feedObj.getString("location")));
                            //contentPrice.setText(String.valueOf("Rp. " + moneyFormater(feedObj.getString("basic_price"))));
                            //cashbackValue.setText(String.valueOf("Cashback up to\nRp. " + moneyFormater(feedObj.getString("max_cashback"))));
                            //contentCashback.setText(String.valueOf( "Cashback "+feedObj.getString("cashback_percentage") + " %"));
                            //sold.setText(String.valueOf(feedObj.getString("soldout") + "\n" + "terjual"));
                            //quantity.setText(String.valueOf(feedObj.getString("quantity") + "\n" + "tersisa"));
                            //contentCategories.setText(feedObj.getString("category"));

                            //priceValue = feedObj.getInt("basic_price");
                            //totalPiceValue = priceValue;
                            //totalPrice.setText(String.valueOf("Rp. " + moneyFormater(String.valueOf(priceValue))));
                            //contentDetail.setText(feedObj.getString("promo_desc"));
                            //JSONObject imageObj = (JSONObject) feedObj.getJSONObject("images");
                            //JSONArray thumbnail = (JSONArray) imageObj.getJSONArray("thumbImage");
                            Glide.with(getApplicationContext())  //2
                                    .load(AppConfig.MERCHANT_PROFILE_URL + String.valueOf(feedObj.getString("default_image"))) //3
                                    .centerCrop() //4
                                    .placeholder(R.drawable.placeholder) //5
                                    .error(R.drawable.placeholder) //6
                                    //.fallback(R.drawable.ic_no_image) //7
                                    .into(productImage);
                           // JSONArray bigImage = (JSONArray) imageObj.getJSONArray("bigImage");

                           // for (int j = 0; j < bigImage.length(); j++) {
                           //     BannerItems item = new BannerItems();
                            //    item.setPromoTitle(AppConfig.MERCHANT_BASE_URL + String.valueOf(bigImage.get(j)));
                            //    bannerItems.add(item);
                            //}

                        }

                        bannerAdapter.notifyDataSetChanged();
                    } else {
                        //swipeContainer.setRefreshing(false);
                        String errorMsg = jObj.getString("message");
                        //Toast.makeText(getContext(),
                        //      errorMsg , Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    //Toast.makeText(MerchantDetailActivity.this, "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                    //swipeContainer.setRefreshing(false);
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Banner error: " + error.getMessage());
               // Toast.makeText(MerchantDetailActivity.this,
                 //       error.getMessage(), Toast.LENGTH_LONG).show();
                // hideDialog();
                //swipeContainer.setRefreshing(false);
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    public void setupContentThree(){
        contentThreeItems = new ArrayList<PromoItems>();
        recyclerViewLayoutManager1 = new GridLayoutManager(MerchantDetailActivity.this,1);
        contentThreeView.setLayoutManager(recyclerViewLayoutManager1);
        contentThreeAdapter = new ContentThreeViewAdapter(MerchantDetailActivity.this, contentThreeItems);
        contentThreeView.setAdapter(contentThreeAdapter);
        getDataContentThree(getIntent().getExtras().getString("id"));
       // getDataContentThree("59");
    }

    private void getDataContentThree(String type) {
        // Tag used to cancel the request
        String tag_string_req = "content_req";

        StringRequest strReq = new StringRequest(Request.Method.GET,
                AppConfig.URL_ADD_WISHLIST + "merchant_id=" + type + "/page=1/limit=10/sort=desc", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Login Response: " + response.toString());

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean success = jObj.getBoolean("success");
                    //int count = jObj.getInt("count");

                    // Check for error node in json
                    if (success) {
                        JSONArray feedArray = jObj.getJSONArray("data");

                        for (int i = 0; i < feedArray.length(); i++) {
                            JSONObject feedObj = (JSONObject) feedArray.get(i);
                            PromoItems item = new PromoItems();
                            item.setId(feedObj.getString("id"));
                            item.setTitle(feedObj.getString("promo_title"));
                            item.setMerchantName(feedObj.getString("merchant_title"));
                            item.setAddress(feedObj.getString("merchant_address") + ", " + feedObj.getString("merchant_location"));
                            item.setBasicPrice(feedObj.getString("basic_price"));
                            item.setPrice(feedObj.getString("promo_price"));
                            item.setCashBack(feedObj.getString("cashback_percentage"));
                            item.setBenefit(feedObj.getString("benefit"));
                            item.setSnk(feedObj.getString("tnc"));
                            JSONObject imageObj = (JSONObject) feedObj.getJSONObject("images");
                            item.setSlug(feedObj.getString("slug"));
                            JSONArray thumbnail = (JSONArray) imageObj.getJSONArray("thumbImage");
                            item.setImageUrl(AppConfig.MERCHANT_BASE_URL + String.valueOf(thumbnail.get(0)));
                            Glide.with(getApplicationContext())  //2
                                    .load(AppConfig.MERCHANT_BASE_URL + String.valueOf(thumbnail.get(0))) //3
                                    .centerCrop() //4
                                    .placeholder(R.drawable.placeholder) //5
                                    .error(R.drawable.placeholder) //6
                                    //.fallback(R.drawable.ic_no_image) //7
                                    .into(rvBanner);
                            contentThreeItems.add(item);
                        }

                        contentThreeAdapter.notifyDataSetChanged();
                    } else {
                        //swipeContainer.setRefreshing(false);
                        String errorMsg = jObj.getString("message");
                        //Toast.makeText(getContext(),
                        //      errorMsg , Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                   // Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                    //swipeContainer.setRefreshing(false);
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Banner error: " + error.getMessage());
                //Toast.makeText(getApplicationContext(),
                 //       error.getMessage(), Toast.LENGTH_LONG).show();
                // hideDialog();
                //swipeContainer.setRefreshing(false);
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }



    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()){
            case R.id.toolbar_edit:
                Intent intent = new Intent(MerchantDetailActivity.this, ProfileActivity.class);
                startActivityForResult(intent, 1);
                MerchantDetailActivity.this.overridePendingTransition(R.transition.bottom_up, R.transition.nothing);

                break;


        }
        return false;
    }

    public String moneyFormater (String value){
        try {
            NumberFormat formatter = new DecimalFormat("#,###");
            String formattedNumber = formatter.format(Double.parseDouble(value));
            return formattedNumber;
        } catch (Exception e) {
            return value ;
        }
    }
}

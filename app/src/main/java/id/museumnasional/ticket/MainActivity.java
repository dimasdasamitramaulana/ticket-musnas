package id.museumnasional.ticket;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.squareup.picasso.Picasso;

import java.util.HashMap;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import id.museumnasional.ticket.helper.PicassoCircleTransformation;
import id.museumnasional.ticket.helper.SQLiteHandler;
import id.museumnasional.ticket.helper.SessionManager;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();
    private LinearLayout btnScan, btnTransaksi, btnProfil, btnBantuan, btnLaporan, btnLogout, adminView;
    public static final String ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION";
    private SQLiteHandler db;
    private SessionManager session;
    private TextView userName, userEmail, userNip;
    ImageView profileImage;
    Context context;
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        db = new SQLiteHandler(MainActivity.this);
        HashMap<String, String> user = db.getUserDetails();
        session = new SessionManager(MainActivity.this);
        context = this;
        userName = findViewById(R.id.user_name);
        userEmail = findViewById(R.id.user_email);
        userNip = findViewById(R.id.nip);
        profileImage = findViewById(R.id.profile_image);
        adminView = findViewById(R.id.admin_view);

        userName.setText(user.get("name"));
        userEmail.setText(user.get("email"));
        userNip.setText("NIP : " + user.get("status"));
        if(!user.get("code").equalsIgnoreCase("admin")){
            adminView.setVisibility(View.GONE);
        }
        Picasso.with(MainActivity.this).load(user.get("image"))
                .error(R.drawable.ic_profil)
                .transform(new PicassoCircleTransformation())
                .into(profileImage);
        btnScan = findViewById(R.id.btn_scan);
        btnScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ScanActivity.class);
                intent.putExtra("EXTRA_CODE", "scan_masuk");
                startActivityForResult(intent, 1);
                MainActivity.this.overridePendingTransition(R.transition.bottom_up, R.transition.nothing);
            }
        });
        btnProfil = findViewById(R.id.btn_profile);
        btnProfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ScanActivity.class);
                intent.putExtra("EXTRA_CODE", "scan_keluar");
                startActivityForResult(intent, 1);
                MainActivity.this.overridePendingTransition(R.transition.bottom_up, R.transition.nothing);
            }
        });
        btnBantuan = findViewById(R.id.btn_print);
        btnBantuan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, TestPrintActivity.class);
                startActivityForResult(intent, 1);
                MainActivity.this.overridePendingTransition(R.transition.bottom_up, R.transition.nothing);
            }
        });
        btnLaporan = findViewById(R.id.btn_order);
        btnLaporan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, HomeActivity.class);
                startActivityForResult(intent, 1);
                MainActivity.this.overridePendingTransition(R.transition.bottom_up, R.transition.nothing);
            }
        });
        btnLogout = findViewById(R.id.btn_logout);
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setCancelable(true);
                //builder.setTitle("Hapus cache");
                builder.setMessage("Apakah anda ingin logout ?");
                builder.setPositiveButton("Ya",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                logoutUser();
                            }
                        });
                builder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
    }


    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        IsFinish("Apakah anda ingin keluar aplikasi ?");
    }

    public void IsFinish(String alertmessage) {

        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:

                        Intent startMain = new Intent(Intent.ACTION_MAIN);
                        startMain.addCategory(Intent.CATEGORY_HOME);
                        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(startMain);

                        // This above line close correctly
                        //finish();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(alertmessage)
                .setPositiveButton("YA", dialogClickListener)
                .setNegativeButton("TIDAK", dialogClickListener).show();

    }

    private void logoutUser() {
        session.setLogin(false);
        db.deleteUsers();
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
        MainActivity.this.overridePendingTransition(R.transition.bottom_up, R.transition.nothing);
        MainActivity.this.finish();
    }
}

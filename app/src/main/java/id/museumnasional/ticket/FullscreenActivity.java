package id.museumnasional.ticket;

import android.annotation.SuppressLint;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import es.rcti.printerplus.printcom.models.PrintTool;
import es.rcti.printerplus.printcom.models.StructReport;

import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class FullscreenActivity extends AppCompatActivity {

    private Button tiket, back;
    private String mCurrentUrl = "";
    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_fullscreen);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        tiket = findViewById(R.id.ticket);
        back = findViewById(R.id.back);
        webView = findViewById(R.id.web_view);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);

        //webView.setWebChromeClient(new WebChromeClient());
        webView.setWebChromeClient(new WebChromeClient(){
            @Override
            public void onReceivedTitle(WebView view, String title) {
                getWindow().setTitle(title); //Set Activity tile to page title.
            }
        });

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return false;
            }
        });
        //webView.setWebViewClient(new WebViewClient());
      //  webView.getSettings().setJavaScriptEnabled(true);

        webView.loadUrl("https://tiket.museumnasional.or.id/");
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        tiket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                printTicket();
            }
        });
    }

    private void  printTicket(){
        String url = webView.getUrl();
        if(url.contains("invoice")){
            String booking_code = url.substring(url.lastIndexOf("/"));
            tiket.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    StructReport msr = new StructReport();
                    msr.addItemAlignment( StructReport.ALIGNMENT_CENTER );
                    msr.addItemSizeFont(StructReport.SIZE_FONT_2);
                    msr.addText("MUSEUM NASIONAL INDONESIA\n");
                    msr.addItemSizeFont(StructReport.SIZE_FONT_1);
                    msr.addText("jl. Medan Merdeka Barat No.12 - Jakarta, Indonesia");
                    msr.addText("021 3868172 - 0813 5000 7257\n");
                    //msr.addBarcodeHRI( StructReport.BARCODE_HRI_BELOW );
                    //msr.addBarcodeData( "1234567890128" );
                   // Toast.makeText(FullscreenActivity.this, webView.getUrl() , Toast.LENGTH_SHORT).show();
                    Bitmap QRBit = printQRCode(booking_code);
                    msr.addImageBitmap(QRBit);
                    msr.addItemSizeFont(StructReport.SIZE_FONT_2);
                    msr.addText(booking_code + "\n");

                    msr.addItemAlignment(StructReport.ALIGNMENT_CENTER);
                    msr.addText("www.museumnasional.or.id\n\n\n");
                    msr.addCut();
                    PrintTool.sendOrder(FullscreenActivity.this, msr);
                }
            });
        } else {
            // tiket.setVisibility(View.GONE);
            Toast.makeText(FullscreenActivity.this, "Silahkan selesaikan transaksi anda terlebih dahulu" , Toast.LENGTH_SHORT).show();
        }
    }

    public class myWebClient extends WebViewClient
    {
        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            //Toast.makeText(FullscreenActivity.this, url , Toast.LENGTH_SHORT).show();
            view.loadUrl("javascript:window.android.onUrlChange(window.location.href);");
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
            if(url.contains("invoice")){
               // Toast.makeText(FullscreenActivity.this, "VISIBLE" , Toast.LENGTH_SHORT).show();
                //tiket.setVisibility(View.VISIBLE);
                String booking_code = url.substring(url.lastIndexOf("/"));
                tiket.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        StructReport msr = new StructReport();
                        msr.addItemAlignment( StructReport.ALIGNMENT_CENTER );
                        msr.addItemSizeFont(StructReport.SIZE_FONT_2);
                        msr.addText("MUSEUM NASIONAL INDONESIA\n");
                        msr.addItemSizeFont(StructReport.SIZE_FONT_1);
                        msr.addText("jl. Medan Merdeka Barat No.12 - Jakarta, Indonesia");
                        msr.addText("021 3868172 - 0813 5000 7257\n");
                        //msr.addBarcodeHRI( StructReport.BARCODE_HRI_BELOW );
                        //msr.addBarcodeData( "1234567890128" );
                        Toast.makeText(FullscreenActivity.this, booking_code , Toast.LENGTH_SHORT).show();
                        Bitmap QRBit = printQRCode(booking_code);
                        msr.addImageBitmap(QRBit);
                        msr.addItemSizeFont(StructReport.SIZE_FONT_2);
                        msr.addText(booking_code + "\n");

                        msr.addItemAlignment(StructReport.ALIGNMENT_CENTER);
                        msr.addText("www.museumnasional.or.id\n\n\n");
                        msr.addCut();
                        PrintTool.sendOrder(FullscreenActivity.this, msr);
                    }
                });
            } else {
               // tiket.setVisibility(View.GONE);
                //Toast.makeText(FullscreenActivity.this, "GONE" , Toast.LENGTH_SHORT).show();
            }

            //Toast.makeText(FullscreenActivity.this, url , Toast.LENGTH_SHORT).show();
        }


        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub
            view.loadUrl(url);
            //Toast.makeText(FullscreenActivity.this, url , Toast.LENGTH_SHORT).show();
            return true;
        }
    }

    private Bitmap printQRCode(String textToQR){
        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        try {
            BitMatrix bitMatrix = multiFormatWriter.encode(textToQR, BarcodeFormat.QR_CODE,300,300);
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
            return bitmap;
        } catch (WriterException e) {
            e.printStackTrace();
            return null;
        }
    }

}
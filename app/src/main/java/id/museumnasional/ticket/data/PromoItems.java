package id.museumnasional.ticket.data;

public class PromoItems {

    private String id, imageUrl, title, detail, merchantName, merchantImage, categories, address, location,basicPrice, price, cashBack, slug, benefit, snk, quantity;
    private Integer hotDeal;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String image_url) {
        this.imageUrl = image_url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetail() {
        return detail;
    }

    public void setMerchantName(String merchant_name) {
        this.merchantName = merchant_name;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantImage(String merchant_image) {
        this.merchantImage = merchant_image;
    }

    public String getMerchantImage() {
        return merchantImage;
    }

    public void setCategories(String categories) {
        this.categories = categories;
    }

    public String getCategories() {
        return categories;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getBasicPrice() {
        return basicPrice;
    }

    public void setBasicPrice(String basic_price) {
        this.basicPrice = basic_price;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCashBack() {
        return cashBack;
    }

    public void setCashBack(String cashback) {
        this.cashBack = cashback;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getBenefit() {
        return benefit;
    }

    public void setBenefit(String benefit) {
        this.benefit = benefit;
    }

    public String getSnk() {
        return snk;
    }

    public void setSnk(String snk) {
        this.snk = snk;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public Integer getHotDeal() {
        return hotDeal;
    }

    public void setQHotDeal(Integer hotdeal) {
        this.hotDeal = hotdeal;
    }




}

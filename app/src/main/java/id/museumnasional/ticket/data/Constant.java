package id.museumnasional.ticket.data;

public class Constant {
	public static String mBarCodeData = "123456789";
	public static String mBmpPathData = "/storage/sdcard0/DCIM/test.bmp";
	public static String mBmpPath = "/storage/sdcard0/DCIM/";
	public static String WebAddress = "Welcome";
	
	public static String m_strUS1 = "Normal";
	public static String m_strUS2 = "Out of paper";
	public static String m_strUS3 = "Abnormal";
	public static String m_strUS4 = "Gets status failure";

	public static String TESTDATA_US = "Shenzhen Meisong Technology Co., Ltd. was founded in 2006, "
		 	+ "2013 by the state as a high-tech enterprises, through the "
		 	+ "ISO9001 quality system certification.Headquartered in "
		 	+ "Shenzhen (Shenzhen), the establishment of branches in Beijing,"
		 	+ "(India), Iran, Britain and other places have agents." + "\n";

	public static String m_PrintDataUS = "Small panel structure, using the latest ARM design "
			+ " and printers movement in the same institution, reduce the space of the installation, the "
			+ " installation of improve product reliability; The design is exquisite, stable performance "
			+ " and fashion appearance. With automatic feed, automatic paper cutting, paper detection, "
			+ " and other functions, to make the end user to use more simple and smooth\n"
			+ "Interface:Serial port(RS232 or TTL),USB\n\n\n";
	
	// 小票打印文字中英文对照
	public static int ADD_NUM = 1000;
	public static String TITLE_CN = "中国农业银行\n\n" + "办理业务(一)\n\n";
	public static String TITLE_US = "Agricultural Bank China\n\n" + "Transact business (1)\n\n";
	public static String QUEUE_NUMBER = String.valueOf(ADD_NUM) + "\n\n";
	public static String STRDATA_CN = "您前面有 10 人等候，请注意叫号\n\n"
										+ "欢迎光临！我们将竭诚为你服务。\n";
	public static String STRDATA_US = "There are 10 people waiting in front of you, please note the number\n\n"
										+ "Welcome! We will serve you wholeheartedly.\n";

	/*
	 *  0 打印机正常 、1 打印机未连接或未上电、2 打印机和调用库不匹配 
	 *  3 打印头打开 、4 切刀未复位 、5 打印头过热 、6 黑标错误 、7 纸尽 、8 纸将尽
	 */
	public static String Receive_US = "Receive[Hex]:";
	public static String State_US = "\r\nState:";
	public static String Normal_US = "  Normal;";
	public static String NoConnectedOrNoOnPower_US = "  Printer is not connected or not on power;";
	public static String PrinterAndLibraryNotMatch_US = "  Printer and library does not match;";
	public static String PrintHeadOpen_US = "  Print head open;";
	public static String CutterNotReset_US = "  Cutter not reset;";
	public static String PrintHeadOverheated_US = "  Print head overheated;";
	public static String BlackMarkError_US = "  Black mark error;";
	public static String PaperExhausted_US = "  PaperExhausted;";
	public static String PaperWillExhausted_US = "  Paper will exhausted;";
	public static String Abnormal_US = "  Abnormal;";

	public static String LoadBmpPath = "/storage/sdcard0/DCIM/test1.bmp;" +
								"/storage/sdcard0/DCIM/test2.bmp;" +
								"/storage/sdcard0/DCIM/test3.bmp;" +
								"/storage/sdcard0/DCIM/small2v.bmp;" +
								"/storage/sdcard0/DCIM/logo2.bmp;" +
								"/storage/sdcard0/DCIM/logo3.bmp;";

	public static String LoadBmpPath2 = "/storage/sdcard0/DCIM/logo2.bmp;" +
								 "/storage/sdcard0/DCIM/logo3.bmp;" +
			                     "/storage/sdcard0/DCIM/small2v.bmp;" +
			                     "/storage/sdcard0/DCIM/test3.bmp;" +
			                     "/storage/sdcard0/DCIM/test1.bmp;" +
			                     "/storage/sdcard0/DCIM/test2.bmp;";
	public static String LoadBmpPath3 = "/mnt/sdcard/DCIM/BMP/logo1.bmp;" +
			 "/mnt/sdcard/DCIM/BMP/logo2.bmp;" +
            "/mnt/sdcard/DCIM/BMP/logo3.bmp;";
}

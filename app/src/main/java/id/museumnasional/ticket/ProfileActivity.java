package id.museumnasional.ticket;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import id.museumnasional.ticket.helper.PicassoCircleTransformation;
import id.museumnasional.ticket.helper.SQLiteHandler;
import id.museumnasional.ticket.helper.SessionManager;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

public class ProfileActivity extends AppCompatActivity  implements Toolbar.OnMenuItemClickListener{

    private EditText userName, userEmail, userPhone, userLocation, userDesc;
    private SQLiteHandler db;
    private SessionManager session;
    ImageView profileImage;
    TextView btnUploadImage;
    final int CODE_GALLERY_REQUEST = 999;
    Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        toolbar.inflateMenu(R.menu.toolbar_menu_edit_profile);
        toolbar.setOnMenuItemClickListener(this);

        btnUploadImage = findViewById(R.id.btn_upload_image);
        btnUploadImage.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ActivityCompat.requestPermissions(ProfileActivity.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, CODE_GALLERY_REQUEST);
            }
        });

        userName = findViewById(R.id.name);
        userPhone = findViewById(R.id.phone);
        userEmail = findViewById(R.id.email);
        userLocation = findViewById(R.id.location);
        userDesc = findViewById(R.id.deskripsi);

        db = new SQLiteHandler(ProfileActivity.this);
        HashMap<String, String> user = db.getUserDetails();
        session = new SessionManager(getApplicationContext());
        profileImage = findViewById(R.id.profile_image);
        checkSession();
    }

    private void checkSession(){
        if (session.isLoggedIn()) {
            //buttonLogoutView.setVisibility(View.VISIBLE);
            // Fetching user details from sqlite
            HashMap<String, String> user = db.getUserDetails();
            userName.setText(user.get("name"));
            userEmail.setText(user.get("email"));
            userPhone.setText(user.get("phone"));
            userLocation.setText(user.get("status"));
            userDesc.setText(user.get("end_date"));
            Picasso.with(ProfileActivity.this).load(user.get("image"))
                    .error(R.drawable.photo_placeholder)
                    .transform(new PicassoCircleTransformation())
                    .into(profileImage);
        }
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == CODE_GALLERY_REQUEST){
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, "Pilih foto profil"), CODE_GALLERY_REQUEST);
            }else{
                Toast.makeText(getApplicationContext(), "You don't have permission to access gallery!", Toast.LENGTH_LONG).show();
            }
            return;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private String imagetoString(Bitmap bitmap)
    {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 200, byteArrayOutputStream);
        byte[] imageType = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(imageType, Base64.DEFAULT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CODE_GALLERY_REQUEST && resultCode == RESULT_OK && data != null){
            Uri path = data.getData();
            try {
                InputStream inputStream = getContentResolver().openInputStream(path);
                bitmap = BitmapFactory.decodeStream(inputStream);
                Picasso.with(ProfileActivity.this).load(path)
                        .error(R.drawable.photo_placeholder)
                        //.transform(new PicassoCircleTransformation())
                        //.centerCrop()
                        .into(profileImage);
            } catch (IOException e) {
                e.printStackTrace();
            }
            //Toast.makeText(EditProfileActivity.this, "o", Toast.LENGTH_SHORT).show();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {

        switch (item.getItemId()){
            case R.id.toolbar_save:
                AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
                builder.setCancelable(true);
                builder.setMessage("Apakah anda ingin simpan perubahan data ?");
                builder.setPositiveButton("Simpan",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                builder.setNegativeButton("Kembali",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });

                AlertDialog dialog = builder.create();
                dialog.show();

                break;

        }
        return false;
    }
}

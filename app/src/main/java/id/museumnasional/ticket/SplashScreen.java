package id.museumnasional.ticket;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import java.util.HashMap;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityOptionsCompat;
import id.museumnasional.ticket.helper.CircularReveal;
import id.museumnasional.ticket.helper.SQLiteHandler;
import id.museumnasional.ticket.helper.SessionManager;

public class
SplashScreen extends AppCompatActivity {
    private AppCompatActivity activity;
    private LinearLayout splashView;
    private CircularReveal circularReveal;
    private SQLiteHandler db;
    private SessionManager session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_screen);
        splashView = (LinearLayout) findViewById(R.id.splash_view);
        db = new SQLiteHandler(SplashScreen.this);
        HashMap<String, String> user = db.getUserDetails();
        session = new SessionManager(SplashScreen.this);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                ActivityOptionsCompat activityOptionsCompat = ActivityOptionsCompat
                        .makeSceneTransitionAnimation(SplashScreen.this,splashView,"imageMain");
                checkSession();
            }
        }, 3000L); //3000 L = 3 detik
    }

    private void checkSession(){
        if (session.isLoggedIn()) {
            finish();
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
            overridePendingTransition(R.transition.fade_in, R.transition.fade_out);

        } else {
            finish();
            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            overridePendingTransition(R.transition.fade_in, R.transition.fade_out);
        }
    }


}

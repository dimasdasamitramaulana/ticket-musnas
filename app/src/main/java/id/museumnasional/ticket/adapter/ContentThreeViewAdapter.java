package id.museumnasional.ticket.adapter;

import android.content.Intent;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import id.museumnasional.ticket.PromoDetailActivity;
import id.museumnasional.ticket.R;
import id.museumnasional.ticket.data.PromoItems;


/**
 * Created by Juned on 3/27/2017.
 */

public class ContentThreeViewAdapter extends RecyclerView.Adapter<ContentThreeViewAdapter.MyView> {

    private List<PromoItems> feedItems;
    private AppCompatActivity context;
    public class MyView extends RecyclerView.ViewHolder implements View.OnClickListener{

        public ImageView contentImage;
        public TextView contentTitle, contentDesc,contentBasicPrice,  contentPrice, contentCashback;
        public LinearLayout viewHotdeal;
        public MyView(View view) {
            super(view);
            view.setOnClickListener(this);
            contentImage = (ImageView) view.findViewById(R.id.iv_content_image);
            contentTitle = (TextView) view.findViewById(R.id.iv_content_title);
            contentDesc = (TextView) view.findViewById(R.id.iv_content_desc);
            contentBasicPrice = (TextView) view.findViewById(R.id.iv_content_basic_price);
            contentPrice = (TextView) view.findViewById(R.id.iv_content_price);
            contentCashback = (TextView) view.findViewById(R.id.iv_content_cashback);
            viewHotdeal = (LinearLayout) view.findViewById(R.id.view_hotdeal);

        }

        @Override
        public void onClick(View view) {
            int pos = getPosition();
            feedItems.get(pos).getId();
            Intent intent = new Intent(context, PromoDetailActivity.class);
            intent.putExtra("slug", feedItems.get(pos).getSlug());
            intent.putExtra("benefit", feedItems.get(pos).getBenefit());
            intent.putExtra("snk", feedItems.get(pos).getSnk());
            intent.putExtra("merchant_title", feedItems.get(pos).getMerchantName());
            context.startActivityForResult(intent, 999);

        }
    }


    public ContentThreeViewAdapter(AppCompatActivity context, List<PromoItems> horizontalList) {
        this.feedItems = horizontalList;
        this.context = context;
    }

    @Override
    public MyView onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_three_item_layout, parent, false);

        return new MyView(itemView);
    }

    @Override
    public void onBindViewHolder(final MyView holder, final int position) {

        final PromoItems item = feedItems.get(position);
        holder.contentTitle.setText(item.getTitle());
        holder.contentDesc.setText(item.getMerchantName());
        holder.contentBasicPrice.setText(String.valueOf("Rp. " + moneyFormater(item.getBasicPrice())));
        holder.contentBasicPrice.setPaintFlags(holder.contentBasicPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        holder.contentPrice.setText(String.valueOf("Rp. " + moneyFormater(item.getPrice())));
        holder.contentCashback.setText(String.valueOf("Cashback " + item.getCashBack() + "%"));
        try{
            if(item.getHotDeal()==0){
                holder.viewHotdeal.setVisibility(View.GONE);
            } else {
                holder.viewHotdeal.setVisibility(View.VISIBLE);
            }
        } catch (Exception e){
            //return;
        }
        Glide.with(context)  //2
                .load(item.getImageUrl()) //3
                .centerCrop() //4
                .placeholder(R.drawable.placeholder) //5
                .error(R.drawable.placeholder) //6
                //.fallback(R.drawable.ic_no_image) //7
                .into(holder.contentImage); //8

    }

    @Override
    public int getItemCount() {
        return feedItems.size();
    }

    public String moneyFormater (String value){
        try {
            NumberFormat formatter = new DecimalFormat("#,###");
            String formattedNumber = formatter.format(Double.parseDouble(value));
            return formattedNumber;
        } catch (Exception e) {
            return value ;
        }
    }

}

package id.museumnasional.ticket.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.stfalcon.imageviewer.StfalconImageViewer;
import com.stfalcon.imageviewer.loader.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import androidx.viewpager.widget.PagerAdapter;
import id.museumnasional.ticket.R;
import id.museumnasional.ticket.data.BannerItems;


/**
 * Created by dimasdasa on 1/16/18.
 */

public class ViewImagePagerAdapter extends PagerAdapter {
    Context c;
    private LayoutInflater inflater;
    private List<BannerItems> feedItems;

    public ViewImagePagerAdapter(Context c, List<BannerItems> feedItems) {
        this.feedItems = feedItems;
        this.c = c;
    }

    @Override
    public int getCount() {
        return feedItems.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == (object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        inflater = (LayoutInflater)c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(R.layout.image_banner_item_layout, container,
                false);
        final ImageView bannerImage = (ImageView) viewLayout
                .findViewById(R.id.iv_banner_image);
        BannerItems item = feedItems.get(position);
        Glide.with(c)  //2
                .load(item.getImageUrl()) //3
                .centerCrop() //4
                .placeholder(R.drawable.placeholder_long) //5
                .error(R.drawable.placeholder_long) //6
                //.fallback(R.drawable.ic_no_image) //7
                .into(bannerImage); //8

        (container).addView(viewLayout);
        bannerImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = position;
                //c.startActivity(intent);
                List<String> PATH = new ArrayList<>();
                for (int i = 0; i < getCount(); i++) {
                    PATH.add(feedItems.get(i).getImageUrl());
                }

                new StfalconImageViewer.Builder<>(c, PATH, new ImageLoader<String>() {
                    @Override
                    public void loadImage(ImageView imageView, String image) {
                        Glide.with(c)
                                .load(image)
                                .into(imageView);
                    }

                })
                        .withTransitionFrom(bannerImage)
                        .withStartPosition(pos)
                        .show();
            }
        });
        //Toast.makeText(c, item.getPromoTitle(), Toast.LENGTH_LONG).show();

        return viewLayout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        (container).removeView((RelativeLayout) object);
    }


}
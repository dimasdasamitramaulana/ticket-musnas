package id.museumnasional.ticket.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.stfalcon.imageviewer.StfalconImageViewer;
import com.stfalcon.imageviewer.loader.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import id.museumnasional.ticket.R;
import id.museumnasional.ticket.data.BannerItems;


/**
 * Created by Juned on 3/27/2017.
 */

public class PromoImageViewAdapter extends RecyclerView.Adapter<PromoImageViewAdapter.MyView> {

    private List<BannerItems> feedItems;
    private AppCompatActivity context;
    public class MyView extends RecyclerView.ViewHolder implements View.OnClickListener{

        public ImageView bannerImage;
        public MyView(View view) {
            super(view);
            view.setOnClickListener(this);
            bannerImage = (ImageView) view.findViewById(R.id.iv_banner_image);

        }

        @Override
        public void onClick(View view) {
            int pos = getPosition();
            feedItems.get(pos).getId();
            List<String> PATH = new ArrayList<>();
            for (int i = 0; i < getItemCount(); i++) {
                PATH.add(feedItems.get(i).getImageUrl());
            }

            new StfalconImageViewer.Builder<>(context, PATH, new ImageLoader<String>() {
                @Override
                public void loadImage(ImageView imageView, String image) {
                    Glide.with(context)
                            .load(image)
                            .into(imageView);
                }

            })
                    .withTransitionFrom(bannerImage)
                    .withStartPosition(pos)
                    .show();

        }
    }


    public PromoImageViewAdapter(AppCompatActivity context, List<BannerItems> horizontalList) {
        this.feedItems = horizontalList;
        this.context = context;
    }

    @Override
    public MyView onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.promo_image_item_layout, parent, false);

        return new MyView(itemView);
    }

    @Override
    public void onBindViewHolder(final MyView holder, final int position) {

        final BannerItems item = feedItems.get(position);
        Glide.with(context)  //2
                .load(item.getImageUrl()) //3
                .centerCrop() //4
                .placeholder(R.drawable.placeholder_long) //5
                .error(R.drawable.placeholder_long) //6
                //.fallback(R.drawable.ic_no_image) //7
                .into(holder.bannerImage); //8

    }

    @Override
    public int getItemCount() {
        return feedItems.size();
    }

}

package id.museumnasional.ticket.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;

import java.util.List;

import androidx.core.app.ActivityOptionsCompat;
import androidx.viewpager.widget.PagerAdapter;
import id.museumnasional.ticket.BannerDetailActivity;
import id.museumnasional.ticket.R;
import id.museumnasional.ticket.data.BannerItems;


/**
 * Created by dimasdasa on 1/16/18.
 */

public class ViewPagerAdapter extends PagerAdapter {
    Context c;
    private LayoutInflater inflater;
    private List<BannerItems> feedItems;
    private ImageView bannerImage;

    public ViewPagerAdapter(Context c, List<BannerItems> feedItems) {
        this.feedItems = feedItems;
        this.c = c;
    }

    @Override
    public int getCount() {
        return feedItems.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == (object);
    }

    @Override
    public Object instantiateItem(final ViewGroup container, final int position) {

        inflater = (LayoutInflater)c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(R.layout.banner_item_layout, container,
                false);
        bannerImage = (ImageView) viewLayout
                .findViewById(R.id.iv_banner_image);
        BannerItems item = feedItems.get(position);
        Glide.with(c)  //2
                .load(item.getImageUrl()) //3
                .centerCrop() //4
                .placeholder(R.drawable.placeholder_long) //5
                .error(R.drawable.placeholder_long) //6
                //.fallback(R.drawable.ic_no_image) //7
                .into(bannerImage); //8

        (container).addView(viewLayout);
        bannerImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = position;
                Intent intent = new Intent(c, BannerDetailActivity.class);
                intent.putExtra("banner_id", feedItems.get(pos).getId());
                intent.putExtra("banner_image", feedItems.get(pos).getImageUrl());
                ActivityOptionsCompat activityOptionsCompat = ActivityOptionsCompat
                        .makeSceneTransitionAnimation((Activity) c,container,"imageMain");
                c.startActivity(intent,activityOptionsCompat.toBundle());


            }
        });
        //Toast.makeText(c, item.getPromoTitle(), Toast.LENGTH_LONG).show();

        return viewLayout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        (container).removeView((RelativeLayout) object);
    }


}
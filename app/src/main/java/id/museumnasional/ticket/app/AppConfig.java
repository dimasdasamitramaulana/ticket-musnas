package id.museumnasional.ticket.app;

public class AppConfig {


    //public static String BASE_URL = "https://api.rimember.id/rimv2/";
    public static String BASE_URL = "https://api.museumnasional.or.id/"; // stagging

    public static String BANNER_BASE_URL = "https://cdn.rimember.id/mobile-images/banner/";
    public static String MERCHANT_BASE_URL = "https://cdn.rimember.id/web-images/promo-merchant/";
    public static String MERCHANT_PROFILE_URL = "https://cdn.rimember.id/web-images/merchant/";


    public static String URL_ADD_WISHLIST = BASE_URL+ "user/add/wishlist";

    //auth
    public static String URL_LOGIN = BASE_URL + "apps/auth/technician-user/login";

    //transaction
    public static String URL_ORDER_SCAN_MASUK = BASE_URL+ "apps/users/technician-user/update-in-time-by-order-no";
    public static String URL_ORDER_SCAN_KELUAR = BASE_URL+ "apps/users/technician-user/update-out-time-by-order-no";
}
